<?php

namespace App\Http\Controllers\API\V1;

use App\Entities\Books\Book;
use App\Entities\Books\BooksRepository;
use App\Http\Controllers\API\V1\APIBaseController;
use EMedia\Api\Docs\APICall;
use EMedia\Api\Docs\Param;
use Illuminate\Http\Request;

class BooksAPIController extends APIBaseController
{

    protected $repo;

    public function __construct(BooksRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * list of books
     */
    protected function index(Request $request)
    {
        document(function () {
            return (new APICall())
                ->setGroup('Books')
                ->setName('List')
                ->setParams([
                    'q|Search query',
                    'page|Page number',
                    (new Param('sort_by', 'string', 'new =>`1` ,old =>`2`'))->optional()
                ])
                ->setSuccessExample('{
                     "payload": [
                            {
                                "id": 11,
                                "name": "Miss Amaya Wiegand V",
                                "author": "Milton Blick",
                                "isbn_no": "9798071878031",
                                "published_year": 2020,
                                "created_at": "2021-08-02T07:10:05.000000Z",
                                "updated_at": "2021-08-02T07:10:05.000000Z"
                            },
                            {
                                "id": 14,
                                "name": "Matilde White",
                                "author": "Dimitri Hartmann II",
                                "isbn_no": "9781933494050",
                                "published_year": 2019,
                                "created_at": "2021-08-02T07:10:05.000000Z",
                                "updated_at": "2021-08-02T07:10:05.000000Z"
                            }
                            ],
                            "next_page_url": "http://localhost:8000/api/v1/books?page=2",
                            "path": "http://localhost:8000/api/v1/books",
                            "per_page": 20,
                            "prev_page_url": null,
                            "to": 20,
                            "total": 50
                        },
                        "message": "",
                        "result": true
                }')
                ->setSuccessPaginatedObject(Book::class);
        });


        try {

            $booksQuery = Book::query();

            // Searching
            if ($request->filled('q')) {
                $booksQuery->where('name', 'LIKE', '%' . $request->query('q') . '%');
            }

            // Sorting
            if (!is_null($request->input('sort_by'))) {
                if ($request->input('sort_by') == Book::NEW_BOOKS) {
                    $booksQuery->orderBy('published_year', 'desc');
                } else {
                    $booksQuery->orderBy('published_year', 'asc');
                }
            }

            // Paginating
            return response()->apiSuccessPaginated($booksQuery->paginate($request->query('per_page', 20)));

        } catch (\Exception $e) {

            return response()->apiError($e->getMessage());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     *
     * show book
     */
    public function show($id)
    {
        document(function () {
            return (new APICall())
                ->setGroup('Books')
                ->setName('View')
                ->setSuccessObject(Book::class)
                ->setSuccessExample('{
                 "payload": {
                        "id": 2,
                        "name": "Dr. Alfonzo Wiegand V",
                        "author": "Michale Schneider",
                        "isbn_no": "9784206753830",
                        "published_year": 1977,
                        "created_at": "2021-08-02T07:10:05.000000Z",
                        "updated_at": "2021-08-02T07:10:05.000000Z"
                    },
                    "message": "",
                    "result": true
                }');
        });

        $book = Book::findOrFail($id);

        return response()->apiSuccess($book);
    }

}
